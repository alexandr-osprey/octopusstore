﻿using ApplicationCore.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IMeasurementUnitService
    {
        Task<IEnumerable<MeasurementUnit>> ListAllAsync();
        Task<MeasurementUnit> GetByIdAsync(int id);
    }
}
