﻿using ApplicationCore.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface ICategoryService
    {
        Task<IEnumerable<Category>> ListRootCategoriesAsync();
        Task<IEnumerable<Category>> ListSubcategoriesAsync(int parentId);
        Task<Category> GetByIdAsync(int id);
    }
}
