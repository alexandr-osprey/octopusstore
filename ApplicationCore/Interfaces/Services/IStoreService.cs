﻿using ApplicationCore.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IStoreService
    {
        Task<IEnumerable<Store>> ListAllAsync();
        Task<Store> GetByIdAsync(int id);
        Task<Store> AddAsync(Store entity);
        Task UpdateAsync(Store entity);
        Task DeleteAsync(int id);
    }
}
