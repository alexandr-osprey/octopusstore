﻿using ApplicationCore.Entities;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IFileDetailsService<T> where T  : BaseEntity
    {
        Task<Stream> GetStreamAsync(int id);
        Task<FileDetails<T>> GetByIdAsync(int id);
        Task<IEnumerable<FileDetails<T>>> ListByRelatedIdAsync(int relatedId);
        Task<FileDetails<T>> AddAsync(FileDetails<T> imageDetails, Stream fileStream);
        Task DeleteAsync(int id);
    }
}
