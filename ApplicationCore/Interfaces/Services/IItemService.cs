﻿using ApplicationCore.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IItemService
    {
        Task<IEnumerable<Item>> ListAsync(int pageIndex, int itemsPage, ISpecification<Item> spec);
        Task<Item> GetByIdAsync(int id);
        Task<Item> AddAsync(Item entity);
        Task UpdateAsync(Item entity);
        Task DeleteAsync(int id);
    }
}
