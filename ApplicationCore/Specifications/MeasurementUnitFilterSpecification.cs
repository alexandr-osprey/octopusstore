﻿using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{
    public class MeasurementUnitFilterSpecification : BaseSpecification<MeasurementUnit>
    {
        public MeasurementUnitFilterSpecification(int? id)
            : base(i => (!id.HasValue || i.Id == id))
        {
        }
        public MeasurementUnitFilterSpecification()
            : this(null)
        {

        }
    }
}
