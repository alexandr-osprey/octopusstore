﻿using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{
    public class FileByRelatedIdSpecification <T> : BaseSpecification<FileDetails<T>>
    {
        public FileByRelatedIdSpecification(int? itemId) :
            base(i => (!itemId.HasValue || i.RelatedId == itemId))
        {
        }
    }
}
