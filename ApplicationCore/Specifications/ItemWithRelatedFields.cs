﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Specifications
{
    public class ItemWithRelatedFields : BaseSpecification<Item>
    {
        public ItemWithRelatedFields(int id)
            : base(i => i.Id == id)
        {
            AddInclude(i => i.MeasurementUnit);
            AddInclude(i => i.Store);
            AddInclude(i => i.Category);
            AddInclude(i => i.Brand);
        }
    }
}
