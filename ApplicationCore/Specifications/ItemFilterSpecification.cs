﻿using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{
    public class ItemFilterSpecification : BaseSpecification<Item>
    {
        public ItemFilterSpecification(int? brandId, int? categoryId)
            : base(i => (!brandId.HasValue || i.BrandId == brandId) && 
                (!categoryId.HasValue || i.CategoryId == categoryId))
        {
        }
    }
}
