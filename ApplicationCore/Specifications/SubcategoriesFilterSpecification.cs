﻿using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{
    public class SubcategoriesFilterSpecification : BaseSpecification<Category>
    {
        public SubcategoriesFilterSpecification(int parentId) 
            : base(i => i.ParentCategoryId == parentId)
        {
        }

        /// <summary>
        /// filter root categories
        /// </summary>
        public SubcategoriesFilterSpecification() 
            : base(i => i.ParentCategoryId == 1)
        {
        }
    }
}
