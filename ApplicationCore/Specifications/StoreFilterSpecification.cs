﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Specifications
{
    public class StoreFilterSpecification : BaseSpecification<Store>
    {
        public StoreFilterSpecification(int? id)
            : base(i => (!id.HasValue || i.Id == id))
        {
        }
        public StoreFilterSpecification()
            : this(null)
        {

        }
    }
}
