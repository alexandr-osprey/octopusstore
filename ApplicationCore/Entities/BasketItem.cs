﻿namespace ApplicationCore.Entities
{
    public class BasketItem : BaseEntity
    {
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public int ItemId { get; set; }
        public int StoreId { get; set; }
    }
}
