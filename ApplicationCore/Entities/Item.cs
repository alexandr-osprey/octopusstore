﻿using System.Collections.Generic;

namespace ApplicationCore.Entities
{
    public class Item : BaseEntity
    {
        public string Title { get; set; }
        public int CategoryId { get; set; }
        public int StoreId { get; set; }
        public int BrandId { get; set; }

        public int MeasurementUnitId { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public IEnumerable<FileDetails<Item>> Images { get; set; }

        public Category Category { get; set; }
        public Store Store { get; set; }
        public Brand Brand { get; set; }
        public MeasurementUnit MeasurementUnit { get; set; }
    }
}
