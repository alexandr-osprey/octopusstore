﻿namespace ApplicationCore.Entities.OrderAggregate
{
    public class ItemOrdered // ValueObject
    {
        public ItemOrdered(int itemId, string productName)
        {
            CatalogItemId = itemId;
            ProductName = productName;
        }
        private ItemOrdered()
        {
            // required by EF
        }
        public int CatalogItemId { get; private set; }
        public string ProductName { get; private set; }
    }
}
