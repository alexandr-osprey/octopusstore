﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities
{
    public class Store : BaseEntity
    {
        public string Title { get; set; }
        public string SellerId { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public DateTime RegistrationDate { get; set; }

        private readonly List<Item> _items = new List<Item>();
        public IReadOnlyCollection<Item> Items => _items.AsReadOnly();
    }
}
