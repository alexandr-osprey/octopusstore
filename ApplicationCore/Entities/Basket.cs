﻿using System.Collections.Generic;
using System.Linq;

namespace ApplicationCore.Entities
{
    public class Basket : BaseEntity
    {
        public string BuyerId { get; set; }
        public int StoreId { get; set; }
        private readonly List<BasketItem> _items = new List<BasketItem>();
        public IReadOnlyCollection<BasketItem> Items => _items.AsReadOnly();

        public void AddItem(int itemId, int storeId, decimal unitPrice, int quantity = 1)
        {
            if (!Items.Any(i => i.ItemId == itemId))
            {
                _items.Add(new BasketItem()
                {
                    ItemId = itemId,
                    StoreId = storeId,
                    Quantity = quantity,
                    UnitPrice = unitPrice
                });
                return;
            }
            var existingItem = Items.FirstOrDefault(i => i.ItemId == itemId);
            existingItem.Quantity += quantity;
        }
    }
}
