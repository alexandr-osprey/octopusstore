﻿using System.Collections.Generic;

namespace ApplicationCore.Entities
{
    public class Category : BaseEntity
    {
        public string Title { get; set; }
        public int ParentCategoryId { get; set; }
        public bool CanHaveItems { get; set; }
        public string Description { get; set; }

        public Category ParentCategory { get; set; }
        public List<Category> Subcategories = new List<Category>();

        public  List<Item> Items = new List<Item>();
    }
}
