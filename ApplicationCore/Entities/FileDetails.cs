﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace ApplicationCore.Entities
{
    public class FileDetails<T> : BaseEntity
    {
        public static readonly string rootDirectory = @"C:\files\";

        public FileDetails(string ownerUsername, string contentType, int relatedId)
        {
            ContentType = contentType;
            RelatedId = relatedId;
            OwnerUsername = ownerUsername;
            DirectoryPath =
                Path.Combine(rootDirectory, GetSafeFileName(ownerUsername));

            FullPath = 
                Path.Combine(DirectoryPath, 
                    Guid.NewGuid().ToString() + GetExtension(contentType));
        }
        public string Title { get; set; }
        public string ContentType { get; set; }
        public int  RelatedId { get; set; }
        public string FullPath { get; set; }
        public string DirectoryPath { get; set; }
        public string OwnerUsername { get; set; }

        public T RelatedEntity { get; set; }

        public static string GetSafeFileName(string filename)
        {
            return string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
        }
        public static string GetExtension(string contentType)
        {
            return "." + contentType.Substring(contentType.IndexOf('/') + 1);
        }
        public async Task SaveFileAsync(Stream inputStream)
        {
            if (!Directory.Exists(DirectoryPath))
            {
                Directory.CreateDirectory(DirectoryPath);
            }
            await SaveFile(FullPath, inputStream);
        }
        public  Stream GetStream()
        {
            return File.OpenRead(FullPath);
        }
        private async static Task SaveFile(string fileName, Stream inputStream)
        {
            if (inputStream == null)
            {
                throw new ArgumentNullException();
            }
            using (var outputStream = File.Create(fileName))
            {
                using(inputStream)
                {
                    inputStream.Seek(0, SeekOrigin.Begin);
                    await inputStream.CopyToAsync(outputStream);
                }
            }
        }
    }
}
