﻿namespace ApplicationCore.Entities
{
    public class Brand : BaseEntity
    {
        public string Title { get; set; }
    }
}
