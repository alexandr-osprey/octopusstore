﻿namespace ApplicationCore.Entities
{
    public class MeasurementUnit : BaseEntity
    {
        public string Title { get; set; }
    }
}
