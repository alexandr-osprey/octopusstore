﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Infrastructure.Data;
using Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests.Services
{
    public class StoreServiceTests : ServiceTestsBase<IStoreService>
    {
        public StoreServiceTests(ITestOutputHelper output) 
            : base(output)
        {
            service = new StoreService(new EfRepository<Store>(storeContext));
        }

        [Fact]
        public async Task ListAllAsync()
        {
            var stores = await service.ListAllAsync();
            Assert.Equal(storeContext.Set<Store>().Count(), stores.Count());
        }
        [Fact]
        public async Task GetByIdAsync()
        {
            var expected = await storeContext.Stores.AsNoTracking()
                    .Include(s => s.Items)
                    .FirstOrDefaultAsync();
            var actual = await service.GetByIdAsync(expected.Id);
            Assert.Equal(
                JsonConvert.SerializeObject(expected),
                JsonConvert.SerializeObject(actual));  
        }
        [Fact]
        public async Task AddAsync()
        {
            var store = new Store()
            {
                Address = "ad 1",
                SellerId = "1",
                Title = "testSeller1",
                RegistrationDate = DateTime.Now
            };
            await service.AddAsync(store);
            Assert.True(storeContext.Stores.Contains(store));
        }
        [Fact]
        public async Task UpdateAsync()
        {
            string newTitle = "TestTitle1";
            DateTime newDate = DateTime.Now;
            var store = new Store
            {
                Id = 1,
                Title = newTitle,
                RegistrationDate = newDate,
                SellerId = "1"
            };
            
            store.Title = newTitle;
            store.RegistrationDate = newDate;
            storeContext.Entry(store).State = EntityState.Deleted;
            await service.UpdateAsync(store);
            store = await storeContext.Stores.AsNoTracking().FirstOrDefaultAsync();
            Assert.Equal(store.Title, newTitle);
            Assert.Equal(store.RegistrationDate, newDate);
        }
        [Fact]
        public async Task DeleteAsync()
        {
            var store = await storeContext.Stores.AsNoTracking().FirstOrDefaultAsync();
            await service.DeleteAsync(1);
            Assert.False(storeContext.Stores.Contains(store));
        }
    }
}
