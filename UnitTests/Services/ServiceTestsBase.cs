﻿using Infrastructure.Data;
using Infrastructure.Identity;
using Infrastructure.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit.Abstractions;

namespace UnitTests
{
    public abstract class ServiceTestsBase<T>
    {
        public ServiceTestsBase(ITestOutputHelper output)
        {
            services = new ServiceCollection();
            storeContext = StoreContextMock.ConfigureStoreContext(services, output);
            userManager = AppIdentityMock.ConfigureIdentity(services).Result;
            serviceProvider = services.BuildServiceProvider();
            _output = output;
        }
        public ServiceCollection services;
        public ServiceProvider serviceProvider;
        public UserManager<ApplicationUser> userManager;
        public StoreContext storeContext;
        public T service;
        public ITestOutputHelper _output;
    }
}
