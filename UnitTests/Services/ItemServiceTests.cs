﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Specifications;
using Infrastructure.Data;
using Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests.Services
{
    public class ItemServiceTests : ServiceTestsBase<IItemService>
    {
        public ItemServiceTests(ITestOutputHelper output) 
            : base(output)
        {
            service = new ItemService(new EfRepository<Item>(storeContext));
        }

        [Fact]
        public async Task ListAsync()
        {
            int itemsPerPage = 2;

            var page1Actual = new HashSet<Item> ( 
                await service.ListAsync(1, itemsPerPage, new ItemFilterSpecification(null, null)));
            var page1Expected = new HashSet<Item>(storeContext.Items.Take(2));
            Assert.True(page1Expected.SetEquals(page1Actual));

            var page3Actual = new HashSet<Item>(
                await service.ListAsync(3, itemsPerPage, new ItemFilterSpecification(null, null)));
            var page3Expected = new HashSet<Item>(storeContext.Items.Skip(itemsPerPage * 2).Take(2));
            Assert.True(page3Expected.SetEquals(page3Actual));
        }
        [Fact]
        public async Task GetByIdAsync()
        {
            var expected = JsonConvert.SerializeObject(
                await storeContext.Items.AsNoTracking()
                    .Include(i => i.Brand)
                    .Include(i => i.Category)
                        .ThenInclude(c => c.ParentCategory)
                        //.ThenInclude(c => c.Subcategories)
                    .Include(i => i.MeasurementUnit)
                    .Include(i => i.Store)
                    .SingleOrDefaultAsync(i => i.Id == 2));
            var actual = JsonConvert.SerializeObject(await service.GetByIdAsync(2));
            Assert.Equal(expected, actual);
        }
        [Fact]
        public async Task AddAsync()
        {
            var item = new Item()
            {
                BrandId = 1,
                CategoryId = 1,
                MeasurementUnitId = 1,
                StoreId = 1,
                Title = "testItem1"
            };
            await service.AddAsync(item);
            Assert.True(await storeContext.Items.ContainsAsync(item));
        }
        [Fact]
        public async Task UpdateAsync()
        {
            string newTitle = "newTitle1";
            int newBrandId = 3;
            var item = new Item()
            {
                Id = 1,
                BrandId = newBrandId,
                CategoryId = 1,
                MeasurementUnitId = 1,
                StoreId = 1,
                Title = newTitle
            };
            storeContext.Entry(item).State = EntityState.Deleted;
            await service.UpdateAsync(item);
            var updatedItem = await storeContext.Items.AsNoTracking().SingleAsync(i => i.Id == 1);
            Assert.Equal(newTitle, updatedItem.Title);
            Assert.Equal(newBrandId, updatedItem.BrandId);
        }
        [Fact]
        public async Task DeleteAsync()
        {
            var item = await storeContext.Items.AsNoTracking().FirstOrDefaultAsync();
            await service.DeleteAsync(item.Id);
            Assert.False(storeContext.Items.Contains(item));
        }

    }
}
