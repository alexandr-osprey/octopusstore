﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Infrastructure.Data;
using Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests.Services
{
    public class MeasurementUnitServiceTests : ServiceTestsBase<IMeasurementUnitService>
    {
        public MeasurementUnitServiceTests(ITestOutputHelper output) :
            base(output)
        {
            service = new MeasurementUnitService(new EfRepository<MeasurementUnit>(storeContext));
        }

        [Fact]
        public async Task ListAllAsync()
        {
            var units = await service.ListAllAsync();
            Assert.Equal(storeContext.MeasurementUnits.Count(), units.Count());
        }

    }
}
