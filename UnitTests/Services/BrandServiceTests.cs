﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Infrastructure.Data;
using Infrastructure.Services;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests.Services
{
    public class BrandServiceTests : ServiceTestsBase<IBrandService>
    {
        public BrandServiceTests(ITestOutputHelper output) 
            : base(output)
        {
            service = new BrandService(new EfRepository<Brand>(storeContext));
        }

        [Fact]
        public async Task ListAllAsync()
        {
            var brands = await service.ListAllAsync();
            Assert.Equal(storeContext.Brands.Count(), brands.Count());
        }
    }
}
