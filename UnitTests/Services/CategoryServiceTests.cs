﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Infrastructure.Data;
using Infrastructure.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests.Services
{
    public class CategoryServiceTests : ServiceTestsBase<ICategoryService>
    {
        public CategoryServiceTests(ITestOutputHelper output) 
            : base(output)
        {
            service = new CategoryService(new EfRepository<Category>(storeContext));
        }

        [Fact]
        public async Task ListRootCategoriesAsync()
        {
            HashSet<Category> expectedRootElements =
                storeContext.Categories.Where(c => c.ParentCategoryId == 1).ToHashSet();
            
            HashSet<Category> rootElements = new HashSet<Category>(await service.ListRootCategoriesAsync());
            Assert.True(expectedRootElements.SetEquals(rootElements));
        }

        [Fact]
        public async Task ListSubcategoriesAsync()
        {
            Category parent = storeContext.Categories.Where(
                c => c.ParentCategoryId == 1).FirstOrDefault();
            HashSet<Category> expectedSubcategories = storeContext.Categories.Where(
                c => c.ParentCategoryId == parent.Id).ToHashSet();
            HashSet<Category> actualSubcategories = new HashSet<Category>(await service.ListSubcategoriesAsync(parent.Id));
            Assert.True(expectedSubcategories.SetEquals(actualSubcategories));
            Category categoryWithoutSubcategories = actualSubcategories.ElementAt(1);

            actualSubcategories = new HashSet<Category>(await service.ListSubcategoriesAsync(
                categoryWithoutSubcategories.Id));
            Assert.Empty(actualSubcategories);
        }
    }
}
