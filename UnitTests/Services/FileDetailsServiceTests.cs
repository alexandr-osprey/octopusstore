﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Specifications;
using Infrastructure.Data;
using Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace UnitTests.Services
{
    public class FileDetailsServiceTests : ServiceTestsBase<IFileDetailsService<Item>>
    {
        public FileDetailsServiceTests(ITestOutputHelper output)
            : base(output)
        {
            service = new FileDetailsService<Item>(new EfRepository<FileDetails<Item>>(storeContext));
            //System.Threading.Thread.Sleep(2 * 1000);
        }
        [Fact]
        public async Task GetStreamAsync()
        {
            var details = await storeContext.ItemsImages.FirstOrDefaultAsync();
            using (var stream = details.GetStream())
            {
                byte[] readBytes = new byte[stream.Length];
                int bytesCount = await stream.ReadAsync(readBytes, 0, (int)stream.Length);
                Assert.Equal(stream.Length, bytesCount);
            }
        }
        [Fact]
        public async Task GetByIdAsync()
        {
            var expected = await storeContext.ItemsImages
                //.Include(i => i.RelatedEntity)
                .FirstOrDefaultAsync();
            var actual = await service.GetByIdAsync(expected.Id);
            Assert.Equal(
                JsonConvert.SerializeObject(expected),
                JsonConvert.SerializeObject(actual));
        }
        [Fact]
        public async Task ListByRelatedIdAsync()
        {
            int relatedId = (await storeContext.ItemsImages
                .AsNoTracking()
                .FirstOrDefaultAsync()).RelatedId;
            var expected = storeContext.ItemsImages.Where(
                i => i.RelatedId == relatedId).ToHashSet();

            var actual = new HashSet<FileDetails<Item>>(
                await service.ListByRelatedIdAsync(relatedId));
            Assert.True(expected.SetEquals(actual));
        }
        [Fact]
        public async Task AddAsync()
        {
            var addedDetails = new FileDetails<Item>("testuser", @"image/jpg", 1);
            var firstItemDetails = await storeContext.ItemsImages.AsNoTracking().FirstOrDefaultAsync();
            await service.AddAsync(addedDetails, firstItemDetails.GetStream());
            var fileDest = File.Open(addedDetails.FullPath, FileMode.Open);
            var fileInit = File.Open(firstItemDetails.FullPath, FileMode.Open);
            Assert.Equal(fileInit.Length, fileDest.Length);
        }
        [Fact]
        public async Task DeleteAsync()
        {
            var firstItemDetails = await storeContext.ItemsImages.AsNoTracking().LastOrDefaultAsync();
            await service.DeleteAsync(firstItemDetails.Id);
            Assert.False(storeContext.ItemsImages.Contains(firstItemDetails));
        }

    }
}
