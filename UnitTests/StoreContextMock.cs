using ApplicationCore.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit.Abstractions;

namespace UnitTests
{
    public static class StoreContextMock
    {
        public static StoreContext ConfigureStoreContext(IServiceCollection services, ITestOutputHelper output)
        {
            services.AddDbContext<StoreContext>(c =>
                c.UseInMemoryDatabase(Guid.NewGuid().ToString()));

            var serviceProvider = services.BuildServiceProvider();
            var context = serviceProvider.GetRequiredService<StoreContext>();
            SeedStoreAsync(context, output);
            return context;
        }

        private static void SeedStoreAsync(StoreContext storeContext, ITestOutputHelper output, int? retry = 0)
        {
            int retryForAvailabiltiy = retry.Value;
            try
            {
                if (!storeContext.MeasurementUnits.Any())
                {
                    storeContext.MeasurementUnits.AddRange(GetPreconfiguredMeasurementUnits());
                    storeContext.SaveChanges();
                }
                if (!storeContext.Brands.Any())
                {
                    storeContext.Brands.AddRange(GetPreconfiguredBrands());
                    storeContext.SaveChanges();
                }
                if (!storeContext.Categories.Any())
                {
                    storeContext.Categories.AddRange(GetPreconfiguredCategories());
                    storeContext.SaveChanges();
                }
                if (!storeContext.Items.Any())
                {
                    storeContext.Items.AddRange(GetPreconfiguredItems());
                    storeContext.SaveChanges();
                }
                if (!storeContext.Stores.Any())
                {
                    storeContext.Stores.AddRange(GetPreconfiguredStores());
                    storeContext.SaveChanges();
                }
                if (!storeContext.ItemsImages.Any())
                {
                    storeContext.ItemsImages.AddRange(GetPreconfiguredItemImageDetails(output));
                    storeContext.SaveChanges();
                }

            }
            catch (Exception ex)
            {
            }
        }
        static IEnumerable<MeasurementUnit> GetPreconfiguredMeasurementUnits()
        {
            return new List<MeasurementUnit>()
            {
                new MeasurementUnit { Title = "m" },
                new MeasurementUnit { Title = "kg" },
                new MeasurementUnit { Title = "pcs" }
            };
        }
        static IEnumerable<Brand> GetPreconfiguredBrands()
        {
            return new List<Brand>
            {
                new Brand { Title = "Apple" },
                new Brand { Title = "Samsung" },
                new Brand { Title = "CK" },
                new Brand { Title = "Armani" }
            };
        }
        static IEnumerable<Category> GetPreconfiguredCategories()
        {
            return new List<Category>
            {
                new Category { Title = "root", CanHaveItems = false },
                new Category { Title = "Electronics", CanHaveItems = false, ParentCategoryId = 1 },
                new Category { Title = "Clothes", CanHaveItems = false, ParentCategoryId = 1},
                new Category { Title = "Smartphones", CanHaveItems = true, ParentCategoryId = 2 },
                new Category { Title = "Smartwatches", CanHaveItems = true, ParentCategoryId = 2 },
                new Category { Title = "Shoes", CanHaveItems = true, ParentCategoryId = 3}
            };
        }
        static IEnumerable<Store> GetPreconfiguredStores()
        {
            return new List<Store>
            {
                new Store { Title = "John's store", Address = "NY", Description = "Electronics best deals", SellerId = "john@mail.com" },
                new Store { Title = "Jennifer's store", Address = "Sydney", Description = "Fashion", SellerId = "jennifer@mail.com" }
            };
        }
        static IEnumerable<Item> GetPreconfiguredItems()
        {
            return new List<Item>()
            {
                new Item { Title = "iPhone 6", BrandId = 1, CategoryId = 2, MeasurementUnitId = 3, Price = 1000, StoreId = 1 },
                new Item { Title = "Samsung 7", BrandId = 2, CategoryId = 2, MeasurementUnitId = 3, Price = 700, StoreId = 1 },
                new Item { Title = "Samsung 8", BrandId = 2, CategoryId = 2, MeasurementUnitId = 3, Price = 800, StoreId = 1 },

                new Item { Title = "Shoes", BrandId = 3, CategoryId = 3, MeasurementUnitId = 3, Price = 500, StoreId = 2  },
                new Item { Title = "Jacket", BrandId = 4, CategoryId = 3, MeasurementUnitId = 3, Price = 800, StoreId = 2  },
            };
        }
        static IEnumerable<FileDetails<Item>> GetPreconfiguredItemImageDetails(ITestOutputHelper output)
        {
            ClearFilesDirectory(output);
            string contentType = @"image/jpg";
            string imagesDirectory = @"C:\images";
            var imagesDetails = new List<FileDetails<Item>>
            {
                new FileDetails<Item> ("john@mail.com", contentType, 1) { Title = "iPhone 6" },
                new FileDetails<Item> ("john@mail.com", contentType, 2) { Title = "Samsung 7" },
                new FileDetails<Item> ("john@mail.com", contentType, 3) { Title = "Samsung 8" },
                new FileDetails<Item> ("jennifer@mail.com", contentType, 4) { Title = "Shoes" },
                new FileDetails<Item> ("jennifer@mail.com", contentType, 5) { Title = "Jacket" }
            };

            foreach (var imageDetail in imagesDetails) {
                imageDetail.SaveFileAsync(
                    File.OpenRead(
                        Path.Combine(imagesDirectory, imageDetail.Title + ".jpg"))).Wait();
            };
            return imagesDetails;
        }
        static void ClearFilesDirectory(ITestOutputHelper output)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(FileDetails<Item>.rootDirectory);
                if (di.Exists)
                {
                    foreach (FileInfo file in di.GetFiles()) {  file.Delete(); }
                    foreach (DirectoryInfo dir in di.GetDirectories()) { dir.Delete(true); }
                }
            }
            catch (Exception ex)
            {
                output.WriteLine(ex.Message);
            }
        }
    }
}
