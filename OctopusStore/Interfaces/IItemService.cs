﻿using ApplicationCore.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using OctopusStore.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OctopusStore.Interfaces
{
    public interface IItemService
    {
        Task<ItemIndexViewModel> GetItems(int pageIndex, int itemsPage, int? categoryId, int? brandId );
        Task<IEnumerable<SelectListItem>> GetCategories(int selectedId = 0);
        Task<IEnumerable<SelectListItem>> GetBrands(int selectedId = 0);
        Task<IEnumerable<SelectListItem>> GetStores(int selectedId = 0);
        Task<IEnumerable<SelectListItem>> GetMeasurementUnits(int selectedId = 0);
        
        Task<ItemCreateViewModel> GetCreateViewModelAsync(ItemCreateViewModel viewModel = null);
        Task<ItemDetailsViewModel> GetItemDetailsViewModelAsync(int id);
        Task<ItemDeleteViewModel> GetItemDeleteViewModelAsync(int id);
        Task<ItemEditViewModel> GetItemEditViewModelAsync(int id, ItemEditViewModel viewModel = null);

        Task<Item> CreateItemAsync(ItemCreateViewModel itemModel);
        Task DeleteItemAsync(ItemDeleteViewModel itemModel);
        Task UpdateItemAsync(ItemEditViewModel itemModel);

        Task<ItemImageDetailViewModel> GetItemImageDetailViewModelAsync(int id);
        Task<FileDetails<Item>> CreateItemImageAsync(ItemImageCreateViewModel viewModel);
        Task DeleteItemImageAsync(ItemImageViewModel viewModel);

    }
}
