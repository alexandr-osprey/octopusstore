﻿using OctopusStore.ViewModels;
using System.Threading.Tasks;

namespace OctopusStore.Interfaces
{
    public interface IBasketViewModelService
    {
        Task<BasketViewModel> GetOrCreateBasketForUser(string username);
    }
}
