﻿//using ApplicationCore.Entities;
//using Microsoft.AspNetCore.Mvc.Rendering;
//using Microsoft.Extensions.Caching.Memory;
//using OctopusStore.Interfaces;
//using OctopusStore.ViewModels.Item;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Threading.Tasks;

//namespace OctopusStore.Services
//{
//    public class CachedItemService : IItemService
//    {
//        private readonly IMemoryCache _cache;
//        private readonly ItemService _itemService;
//        private static readonly string _brandsKey = "brands";
//        private static readonly string _categoriesKey = "categories";
//        private static readonly string _itemKey = nameof(Item) + "-";
//        private static readonly string _itemImageKey = "itemImage-{0}";
//        private static readonly string _storesKey = "stores";
//        private static readonly string _measurementUnitKey = "measurementUnits";
//        private static readonly string _itemsKeytemplate = "items-{0}-{1}-{2}-{3}";
//        private static readonly TimeSpan _defaultCacheDuration = TimeSpan.FromSeconds(30);

//        public CachedItemService(IMemoryCache cache,
//            ItemService itemService)
//        {
//            _cache = cache;
//            _itemService = itemService;
//        }

//        public async Task<IEnumerable<SelectListItem>> GetBrands(int selectedId = 0)
//        {
//            return await _cache.GetOrCreateAsync(_brandsKey, async entry =>
//            {
//                entry.SlidingExpiration = _defaultCacheDuration;
//                return await _itemService.GetBrands(selectedId);
//            });
//        }
//        public async Task<ItemIndexViewModel> GetItems(int pageIndex, int itemsPage, int? brandId, int? typeId)
//        {
//            string cacheKey = String.Format(_itemsKeytemplate, pageIndex, itemsPage, brandId, typeId);
//            return await _cache.GetOrCreateAsync(cacheKey, async entry =>
//             {
//                 entry.SlidingExpiration = _defaultCacheDuration;
//                 return await _itemService.GetItems(pageIndex, itemsPage, brandId, typeId);
//             });
//        }
//        public async Task<IEnumerable<SelectListItem>> GetCategories(int selectedId = 0)
//        {
//            return await _cache.GetOrCreateAsync(_categoriesKey, async entry =>
//             {
//                 entry.SlidingExpiration = _defaultCacheDuration;
//                 return await _itemService.GetCategories(selectedId);
//             });
//        }
//        public async Task<IEnumerable<SelectListItem>> GetStores(int selectedId = 0)
//        {
//            return await _cache.GetOrCreateAsync(_storesKey, async entry =>
//            {
//                entry.SlidingExpiration = _defaultCacheDuration;
//                return await _itemService.GetStores(selectedId);
//            });
//        }
//        public async Task<IEnumerable<SelectListItem>> GetMeasurementUnits(int selectedId = 0)
//        {
//            return await _cache.GetOrCreateAsync(_measurementUnitKey, async entry =>
//            {
//                entry.SlidingExpiration = _defaultCacheDuration;
//                return await _itemService.GetMeasurementUnits(selectedId);
//            });
//        }

//        public async Task<ItemCreateViewModel> GetCreateViewModelAsync(ItemCreateViewModel viewModel = null)
//        {
//            return await _itemService.GetCreateViewModelAsync(viewModel);
//        }
//        public async Task<ItemDeleteViewModel> GetItemDeleteViewModelAsync(int id)
//        {
//            return await _itemService.GetItemDeleteViewModelAsync(id);
//        }
//        public async Task<ItemDetailsViewModel> GetItemDetailsViewModelAsync(int id)
//        {
//            return await _cache.GetOrCreate(_itemKey + id, async entry =>
//            {
//                entry.SlidingExpiration = _defaultCacheDuration;
//                return await _itemService.GetItemDetailsViewModelAsync(id);
//            });
//        }
//        public async Task<ItemEditViewModel> GetItemEditViewModelAsync(int id, ItemEditViewModel viewModel = null)
//        {
//            return await _itemService.GetItemEditViewModelAsync(id, viewModel);
//        }

//        public Item CreateItem(ItemCreateViewModel itemModel)
//        {
//            return _itemService.CreateItem(itemModel);
//        }
//        public void UpdateItem(ItemViewModel modelItem)
//        {
//            _itemService.UpdateItem(modelItem);
//        }
//        public void DeleteItem(int id)
//        {
//            _itemService.DeleteItem(id);
//        }

//        public async Task<Image<Item>> GetItemImageAsync(int id)
//        {
//            string cacheKey = String.Format(_itemImageKey, id);
//            return await _cache.GetOrCreate(cacheKey, async entry =>
//            {
//                entry.SlidingExpiration = _defaultCacheDuration;
//                return (await _itemService.GetItemImageAsync(id));
//            });
//        }
//    }
//}
