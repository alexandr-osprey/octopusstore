﻿//using ApplicationCore.Entities;
//using ApplicationCore.Interfaces;
//using ApplicationCore.Specifications;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Mvc.RazorPages;
//using Microsoft.AspNetCore.Mvc.Rendering;
//using Microsoft.Extensions.Logging;
//using OctopusStore.Interfaces;
//using OctopusStore.ViewModels;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace OctopusStore.Services
//{
//    public class ItemService : IItemService
//    {
//        private readonly ILogger<ItemService> _logger;
//        private readonly IAsyncRepository<Item> _itemRepository;
//        private readonly IAsyncRepository<Brand> _brandRepository;
//        private readonly IAsyncRepository<Category> _categoryRepository;
//        private readonly IAsyncRepository<Store> _storeRepository;
//        private readonly IAsyncRepository<MeasurementUnit> _measurementUnitRepository;
//        private readonly IAsyncRepository<Image<Item>> _itemImageRepository;
        
//        public ItemService(
//            ILoggerFactory loggerFactory,
//            IAsyncRepository<Item> itemRepository,
//            IAsyncRepository<Brand> brandRepository,
//            IAsyncRepository<Category> categoryRepository,
//            IAsyncRepository<Store> storeRepository,
//            IAsyncRepository<MeasurementUnit> measurementUnitRepository,
//            IAsyncRepository<Image<Item>> itemImageRepository)
//        {
//            _logger = loggerFactory.CreateLogger<ItemService>();
//            _itemRepository = itemRepository;
//            _brandRepository = brandRepository;
//            _categoryRepository = categoryRepository;
//            _storeRepository = storeRepository;
//            _measurementUnitRepository = measurementUnitRepository;
//            _itemImageRepository = itemImageRepository;
//        }

//        public async Task<ItemIndexViewModel> GetItems(int pageIndex, int itemsPage,  int? categoryId, int? brandId)
//        {
//            _logger.LogInformation("GetStoreItems called");
//            var filterSpecification = new ItemFilterSpecification(brandId, categoryId);
//            var root = await _itemRepository.ListAsync(filterSpecification);

//            var totalItems = root.Count();

//            List<Item> itemsOnPage = root.Skip(itemsPage * pageIndex)
//                .Take(itemsPage)
//                .ToList();
//            var vm = new ItemIndexViewModel()
//            {
//                Items =  itemsOnPage.Select(i => new ItemViewModel()
//                {
//                    Id = i.Id,
//                    Title = i.Title,
//                    Price = i.Price,
//                    Description = i.Description,
//                    MeasurementUnitId = i.MeasurementUnitId,
//                    ImageViewModels = GetItemImageViewModels(i.Id).Result
//                }),
//                Brands = await GetBrands(),
//                Categories = await GetCategories(),
//                BrandFilterApplied = brandId ?? 0,
//                CategoryFilterApplied = categoryId ?? 0,
//                PaginationInfo = new PaginationInfoViewModel()
//                {
//                    ActualPage = pageIndex,
//                    ItemsPerPage = itemsOnPage.Count,
//                    TotalItems = totalItems,
//                    TotalPages = int.Parse(Math.Ceiling(((decimal)totalItems / itemsPage)).ToString())
//                }
//            };

//            vm.PaginationInfo.Next = (vm.PaginationInfo.ActualPage == vm.PaginationInfo.TotalPages - 1) ? "is-disabled" : "";
//            vm.PaginationInfo.Previous = (vm.PaginationInfo.ActualPage == 0) ? "is-disabled" : "";

//            return vm;
//        }
//        public async Task<IEnumerable<SelectListItem>> GetBrands(int selectedId = 0)
//        {
//            _logger.LogInformation("GetBrands called. ");
//            var brands = await _brandRepository.ListAllAsync();
//            var items = new List<SelectListItem>
//            {
//                new SelectListItem() { Value = null, Text = "All"}
//            };
//            foreach (Brand brand in brands)
//            {
//                items.Add(new SelectListItem() {
//                    Value = brand.Id.ToString(), Text = brand.Title,
//                    Selected = brand.Id == selectedId
//                });
//            }
//            return items;
//        }
//        public async Task<IEnumerable<SelectListItem>> GetCategories(int selectedId = 0)
//        {
//            _logger.LogInformation("GetCategories called");
//            var categories = await _categoryRepository.ListAsync(new SubcategoriesFilterSpecification());
//            var items = new List<SelectListItem>
//            {
//                new SelectListItem() { Value = null, Text = "All" }
//            };
//            foreach (Category category in categories)
//            {
//                items.Add(new SelectListItem() {
//                    Value = category.Id.ToString(), Text = category.Title,
//                    Selected = category.Id == selectedId
//                });
//            }

//            return items;
//        }
//        public async Task<IEnumerable<SelectListItem>> GetStores(int selectedId = 0)
//        {
//            _logger.LogInformation("GetStores called. ");
//            var stores = await _storeRepository.ListAllAsync();
//            var items = new List<SelectListItem>
//            {
//                new SelectListItem() { Value = null, Text = "All", Selected = true}
//            };
//            foreach (Store store in stores)
//            {
//                items.Add(new SelectListItem()
//                {
//                    Value = store.Id.ToString(),
//                    Text = store.Title,
//                    Selected = store.Id == selectedId
//                });
//            }
//            items.ElementAt(2).Selected = true;
//            return items;
//        }
//        public async Task<IEnumerable<SelectListItem>> GetMeasurementUnits(int selectedId = 0)
//        {
//            var units = await _measurementUnitRepository.ListAsync(new MeasurementUnitFilterSpecification());
//            var list = new List<SelectListItem>
//            {
//                new SelectListItem() { Value = null, Text = "All", Selected = true }
//            };
//            foreach (MeasurementUnit unit in units)
//            {
//                list.Add(new SelectListItem() {
//                    Value = unit.Id.ToString(), Text = unit.Title,
//                    Selected = unit.Id == selectedId
//                });
//            }
//            return list;
//        }

//        public async Task<ItemCreateViewModel> GetCreateViewModelAsync(ItemCreateViewModel viewModel = null)
//        {
//            if (viewModel == null)
//            {
//                viewModel = new ItemCreateViewModel();
//            }
//            viewModel.Brands = await GetBrands();
//            viewModel.Categories = await GetCategories();
//            viewModel.Stores = await GetStores();
//            viewModel.MeasurementUnits = await GetMeasurementUnits();
            
//            return viewModel;
//        }
//        public async Task<ItemDetailsViewModel> GetItemDetailsViewModelAsync(int id)
//        {
//            var item = await _itemRepository.GetSingleBySpecAsync(new ItemWithRelatedFields(id));
//            if (item == null)
//            {
//                return null;
//            }
//            var detailsModel = new ItemDetailsViewModel(ItemToViewModel(item));
//            detailsModel.ImageViewModels = await GetItemImageViewModels(id);
//            return detailsModel;
//        }
//        public async Task<ItemDeleteViewModel> GetItemDeleteViewModelAsync(int id)
//        {
//            var item = await _itemRepository.GetSingleBySpecAsync(new ItemWithRelatedFields(id));
//            if (item == null)
//            {
//                return null;
//            }
//            var deleteModel = new ItemDeleteViewModel(ItemToViewModel(item));
//            deleteModel.ImageViewModels = await GetItemImageViewModels(id);
//            return deleteModel;

//        }
//        public async Task<ItemEditViewModel> GetItemEditViewModelAsync(int id, ItemEditViewModel viewModel = null)
//        {
//            var item = await _itemRepository.GetSingleBySpecAsync(new ItemWithRelatedFields(id));
//            if (item == null)
//            {
//                return null;
//            }
//            if (viewModel == null)
//            {
//                viewModel = new ItemEditViewModel(ItemToViewModel(item));
//            }
//            viewModel.Brands = await GetBrands(item.BrandId);
//            viewModel.Categories = await GetCategories(item.CategoryId);
//            viewModel.Stores = await GetStores(item.StoreId);
//            viewModel.MeasurementUnits = await GetMeasurementUnits(item.MeasurementUnitId);
//            viewModel.ImageViewModels = await GetItemImageViewModels(id);
//            return viewModel;
//        }

//        public async Task<Item> CreateItemAsync(ItemCreateViewModel itemModel)
//        {
//            if (itemModel == null)
//            {
//                return null;
//            }
//            return await _itemRepository.AddAsync(ViewModelToItem(itemModel));
//        }
//        public async Task UpdateItemAsync(ItemEditViewModel itemModel)
//        {
//            if (itemModel == null)
//            {
//                return;
//            }
//            await _itemRepository.UpdateAsync(ViewModelToItem(itemModel));
//        }
//        public async Task DeleteItemAsync(ItemDeleteViewModel itemModel)
//        {
//            await _itemRepository.DeleteAsync(ViewModelToItem(itemModel));
//        }

//        public async Task<ItemImageDetailViewModel> GetItemImageDetailViewModelAsync(int id)
//        {
//            var image = await _itemImageRepository.GetByIdAsync(id);
//            return new ItemImageDetailViewModel
//            {
//                Id = image.Id,
//                ItemId = image.RelatedId,
//                ImageByte = image.ImageByte,
//                ContentType = image.ContentType,
//                Title = image.Title
//            };  
//        }
//        public async Task DeleteItemImageAsync(ItemImageViewModel viewModel)
//        {
//            var image = await _itemImageRepository.GetByIdAsync(viewModel.Id);
//            if (image == null)
//            {
//                return;
//            }
//            await _itemImageRepository.DeleteAsync(image);
//        }
//        public async Task<Image<Item>> CreateItemImageAsync(ItemImageCreateViewModel viewModel)
//        {
//            var image = new Image<Item>()
//            {
//                ContentType = viewModel.ContentType,
//                ImageByte = viewModel.ImageBytes,
//                RelatedId = viewModel.ItemId,
//                Title = viewModel.Title
//            };
//            return await _itemImageRepository.AddAsync(image);
//        }
//        private async Task<IEnumerable<ItemImageViewModel>> GetItemImageViewModels(int itemId)
//        {
//            return (await _itemImageRepository
//                .ListAsync(new ItemImageSpecification(itemId)))
//                    .Select(i => new ItemImageViewModel
//                    {
//                        Id = i.Id,
//                        ItemId = i.RelatedId,
//                        Title = i.Title
//                    });
//        }
        
//        private Item ViewModelToItem(ItemViewModel itemModel)
//        {
//            if (itemModel == null)
//            {
//                return null;
//            }
//            return new Item()
//            {
//                Id = itemModel.Id,
//                BrandId = itemModel.BrandId,
//                CategoryId = itemModel.CategoryId,
//                Description = itemModel.Description,
//                MeasurementUnitId = itemModel.MeasurementUnitId,
//                StoreId = itemModel.StoreId,
//                Price = itemModel.Price,
//                Title = itemModel.Title
//            };
//        }
//        private ItemViewModel ItemToViewModel(Item item)
//        {
//            if (item == null)
//            {
//                return null;
//            }
//            return new ItemViewModel
//            {
//                Id = item.Id,
//                Title = item.Title,
//                CategoryId = item.CategoryId,
//                BrandId = item.BrandId,
//                Description = item.Description,
//                StoreId = item.StoreId,
//                Price = item.Price,
//                MeasurementUnitId = item.MeasurementUnitId,

//                Brand = item.Brand,
//                Category = item.Category,
//                MeasurementUnit = item.MeasurementUnit,
//                Store = item.Store
//            };
//        }
//    }
//}
