﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace OctopusStore.ViewModels
{
    public class ItemEditViewModel : ItemViewModel
    {
        public ItemEditViewModel()
            : base()
        {
        }
        public ItemEditViewModel(ItemViewModel vm)
            : base(vm)
        {
        }

        [BindProperty]
        public IEnumerable<SelectListItem> Brands { get; set; }
        [BindProperty]
        public IEnumerable<SelectListItem> Categories { get; set; }
        //[BindProperty]
        public IEnumerable<SelectListItem> Stores { get; set; }
        [BindProperty]
        public IEnumerable<SelectListItem> MeasurementUnits { get; set; }
        public int ImageIdToRemove { get; set; }
    }
}
