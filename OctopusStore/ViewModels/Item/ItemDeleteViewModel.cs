﻿namespace OctopusStore.ViewModels
{
    public class ItemDeleteViewModel : ItemViewModel
    {
        public ItemDeleteViewModel() 
            : base()
        {
        }

        public ItemDeleteViewModel(ItemViewModel vm)
            : base(vm)
        {
        }
    }
}
