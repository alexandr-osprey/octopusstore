﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace OctopusStore.ViewModels
{
    public class ItemIndexViewModel
    {
        public IEnumerable<ItemViewModel> Items { get; set; }
        public IEnumerable<SelectListItem> Brands { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
        public int? BrandFilterApplied { get; set; }
        public int? CategoryFilterApplied { get; set; }
        public PaginationInfoViewModel PaginationInfo { get; set; }
    }
}
