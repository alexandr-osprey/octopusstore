﻿using ApplicationCore.Entities;
using Microsoft.AspNetCore.Mvc;

namespace OctopusStore.ViewModels
{
    public class ItemImageViewModel
    {
        [BindProperty]
        public int Id { get; set; }
        public string Title { get; set; }
        public int ItemId { get; set; }
        public string ContentType { get; set; }

        public static ItemImageViewModel FromItemImage(FileDetails<Item> imageItem)
        {
            return new ItemImageViewModel
            {
                Id = imageItem.Id,
                ItemId = imageItem.RelatedId,
                Title = imageItem.Title
            };
        }
    }
}
