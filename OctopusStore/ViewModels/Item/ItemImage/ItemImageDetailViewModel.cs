﻿namespace OctopusStore.ViewModels
{
    public class ItemImageDetailViewModel : ItemImageViewModel
    {
        public byte[] ImageByte { get; set; }
    }
}
