﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System.IO;

namespace OctopusStore.ViewModels
{
    public class ItemImageCreateViewModel : ItemImageViewModel
    {
        public ItemImageCreateViewModel(int itemId)
        {
            this.ItemId = itemId;
        }
        public ItemImageCreateViewModel()
        {
        }
        public IFormFile ImageFormFile { get; set; }
        public byte[] ImageBytes { get; set; }

    }
}
