﻿using ApplicationCore.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OctopusStore.ViewModels
{
    public class ItemViewModel
    {
        public ItemViewModel()
        {
        }
        public ItemViewModel(ItemViewModel vm)
        {
            Id = vm.Id;
            Title = vm.Title;
            CategoryId = vm.CategoryId;
            Category = vm.Category;
            BrandId = vm.BrandId;
            Brand = vm.Brand;
            MeasurementUnitId = vm.MeasurementUnitId;
            MeasurementUnit = vm.MeasurementUnit;
            Price = vm.Price;
            Description = vm.Description;
            StoreId = vm.StoreId;
            Store = vm.Store;
            ImageViewModels = vm.ImageViewModels;
        }
        public int Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Title { get; set; }
        public int CategoryId { get; set; }
        public int StoreId { get; set; }
        public int BrandId { get; set; }
        public int MeasurementUnitId { get; set; }
        public string Description { get; set; }
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
        public IEnumerable<ItemImageViewModel> ImageViewModels { get; set; }

        public Category Category { get; set; }
        public Store Store { get; set; }
        public Brand Brand { get; set; }
        public MeasurementUnit MeasurementUnit { get; set; }
    }
}
