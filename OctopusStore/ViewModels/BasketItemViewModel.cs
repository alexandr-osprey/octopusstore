﻿namespace OctopusStore.ViewModels
{
    public class BasketItemViewModel
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public string ProductTitle { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal OldUnitPrice { get; set; }
        public int Quantity { get; set; }
        public int StoreId { get; set; }
        public string StoreTitle { get; set; }
    }
}
