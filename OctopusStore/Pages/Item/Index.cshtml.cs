﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OctopusStore.Interfaces;
using OctopusStore.ViewModels;

namespace OctopusStore.Pages.Item
{
    public class IndexModel : PageModel
    {
        private readonly IItemService _itemService;

        public IndexModel(IItemService itemService)
        {
            _itemService = itemService;
        }
        public ItemIndexViewModel ItemModel { get; set; } = new ItemIndexViewModel();

        public async Task OnGet(ItemIndexViewModel itemModel, int? pageId)
        {
            ItemModel = await _itemService.GetItems(pageId ?? 0, Constants.ITEMS_PER_PAGE, itemModel.CategoryFilterApplied, itemModel.BrandFilterApplied);
        }
    }
}
