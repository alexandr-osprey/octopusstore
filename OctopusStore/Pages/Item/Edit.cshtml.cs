﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using OctopusStore.Interfaces;
using OctopusStore.ViewModels;

namespace OctopusStore.Pages.Item
{
    public class EditModel : PageModel
    {
        private readonly IItemService _itemService;
        [BindProperty]
        public ItemEditViewModel ItemModel { get; set; }
        public SelectList list { get; set; }

        public EditModel(IItemService itemService)
        {
            _itemService = itemService;
        }

        public async Task<IActionResult> OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ItemModel = await _itemService.GetItemEditViewModelAsync(id.Value);


            if (ItemModel == null)
            {
                return NotFound();
            }


            return Page();
        }

        public async Task<IActionResult> OnPost(ItemEditViewModel itemModel)
        {
            if (!ModelState.IsValid)
            {
                await _itemService.GetItemEditViewModelAsync(itemModel.Id, this.ItemModel);
                return Page();
            }
             await _itemService.UpdateItemAsync(itemModel);

            return RedirectToPage("./Index");
        }
    }
}
