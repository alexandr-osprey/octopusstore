﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OctopusStore.Interfaces;
using OctopusStore.ViewModels;
using System.Threading.Tasks;

namespace OctopusStore.Pages.Item
{
    public class CreateModel : PageModel
    {
        private readonly IItemService _itemService;
        [BindProperty]
        public ItemCreateViewModel ItemModel { get; set; }

        public CreateModel(IItemService itemService)
        {
            _itemService = itemService;
        }

        public async Task OnGet(ItemCreateViewModel itemModel)
        {
            ItemModel = await _itemService.GetCreateViewModelAsync();
        }

        public async Task <IActionResult> OnPost(ItemCreateViewModel itemModel)
        {
            if (!ModelState.IsValid)
            {
                await _itemService.GetCreateViewModelAsync(this.ItemModel);
                return Page();
            }
            var item = _itemService.CreateItemAsync(itemModel);

            return RedirectToPage("./Index");
        }
    }
}