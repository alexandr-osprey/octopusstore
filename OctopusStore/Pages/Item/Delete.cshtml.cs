﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OctopusStore.Interfaces;
using OctopusStore.ViewModels;

namespace OctopusStore.Pages.Item
{
    public class DeleteModel : PageModel
    {
        private readonly IItemService _itemService;
        [BindProperty]
        public ItemDeleteViewModel ItemModel { get; set; }

        public DeleteModel(IItemService itemService)
        {
            _itemService = itemService;
        }



        public async Task<IActionResult> OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ItemModel = await _itemService.GetItemDeleteViewModelAsync(id.Value);

            if (ItemModel == null)
            {
                return NotFound();
            }
            return Page();
        }

        public IActionResult OnPost(int? id)
        {
            if (ItemModel == null)
            {
                return NotFound();
            }

           _itemService.DeleteItemAsync(ItemModel);
            return RedirectToPage("./Index");
        }
    }
}
