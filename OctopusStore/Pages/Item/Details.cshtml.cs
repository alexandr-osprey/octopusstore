﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OctopusStore.Interfaces;
using OctopusStore.ViewModels;
using System.Threading.Tasks;

namespace OctopusStore.Pages.Item
{
    public class DetailsModel : PageModel
    {
        private readonly IItemService _itemService;
        [BindProperty]
        public ItemDetailsViewModel ItemModel { get; set; }

        public DetailsModel(IItemService itemService)
        {
            _itemService = itemService;
        }

        public async Task<IActionResult> OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ItemModel = await _itemService.GetItemDetailsViewModelAsync(id.Value);

            if (ItemModel == null)
            {
                return NotFound();
            }
            return Page();
        }

    }
}
