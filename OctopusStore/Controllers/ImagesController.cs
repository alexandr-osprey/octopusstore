﻿using ApplicationCore.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using OctopusStore.Interfaces;
using OctopusStore.ViewModels;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace OctopusStore.Controllers
{
    public class ImagesController : Controller
    {
        private readonly IItemService _itemService;
        public ImagesController(IItemService itemService)
        {
            _itemService = itemService;
        }

        public async Task<ActionResult> Get(int id)
        {
            var image = await _itemService.GetItemImageDetailViewModelAsync(id);
            if (image != null)
            {
                return Get(image);
            }
            return File(new byte[0], "image/jpeg");
        }
        public async Task<ActionResult> Delete(ItemImageViewModel viewModel)
        {
            await _itemService.DeleteItemImageAsync(viewModel);
            return Redirect($"~/Item/Edit/{viewModel.ItemId}");
        }
        public async Task<ActionResult> Create(ItemImageCreateViewModel viewModel, ModelStateDictionary modelState)
        {
            IFormFile formFile = viewModel.ImageFormFile;
            var fieldDisplayName = string.Empty;

            MemberInfo property = typeof(ItemImageCreateViewModel).GetProperty(formFile.Name.Substring(formFile.Name.IndexOf(".") + 1));
            if (property != null)
            {
                var displayAttribute = property.GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute;

                if (displayAttribute != null)
                {
                    fieldDisplayName = $"{displayAttribute.Name}";
                }
            }
            var fileName = WebUtility.HtmlEncode(Path.GetFileName(formFile.FileName));
            string contentType = formFile.ContentType.ToLower();
            if (contentType != "image/jpeg" && contentType != "image/png")
            {
                modelState.AddModelError(formFile.Name,
                                         $"The {fieldDisplayName}file ({fileName}) must be an image file.");
            }

            if (formFile.Length == 0)
            {
                modelState.AddModelError(formFile.Name, $"The {fieldDisplayName}file ({fileName}) is empty.");
            }
            else if (formFile.Length > 10 * 1024 * 1024)
            {
                modelState.AddModelError(formFile.Name, $"The {fieldDisplayName}file ({fileName}) exceeds 10 MB.");
            }
            else
            {
                try
                {
                    byte[] fileContents = new byte[formFile.Length];
                    using (var reader = formFile.OpenReadStream())
                    {
                        int read = await reader.ReadAsync(fileContents, 0, (int)formFile.Length);

                        if (fileContents.Length == read)
                        {
                            viewModel.ImageBytes = fileContents;
                            viewModel.ContentType = contentType;
                            viewModel.Title = fileName;

                            var image = await _itemService.CreateItemImageAsync(viewModel);
                            if (image != null)
                            {
                                return Redirect($"/Item/Edit/{viewModel.ItemId}");
                            }
                            return Redirect($"/Item/Edit/{viewModel.ItemId}");
                        }
                        else
                        {
                            modelState.AddModelError(formFile.Name,
                                                     $"The {fieldDisplayName}file ({fileName}) is empty.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    modelState.AddModelError(formFile.Name,
                                             $"The {fieldDisplayName}file ({fileName}) upload failed. " +
                                             $"Please contact the Help Desk for support. Error: {ex.Message}");
                }
            }
            return Redirect($"/Item/Edit/{viewModel.ItemId}");
        }

        private ActionResult Get(ItemImageDetailViewModel image)
        {
            return File(image.ImageByte, image.ContentType);
        }
    }
}