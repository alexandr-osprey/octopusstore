﻿using ApplicationCore.Entities.OrderAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ApplicationCore.Entities;

namespace Infrastructure.Data
{

    public class StoreContext : DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {
        }

        public DbSet<Basket> Baskets { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<MeasurementUnit> MeasurementUnits { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<FileDetails<Item>> ItemsImages { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Basket>(ConfigureBasket);
            builder.Entity<Brand>(ConfigureBrand);
            builder.Entity<Category>(ConfigureCategory);
            builder.Entity<Item>(ConfigureItem);
            builder.Entity<Order>(ConfigureOrder);
            builder.Entity<OrderItem>(ConfigureOrderItem);
            builder.Entity<MeasurementUnit>(ConfigureMeasurementUnit);
            builder.Entity<Store>(ConfigureStore);
            builder.Entity<FileDetails<Item>>(ConfigureItemImage);
        }

        private void ConfigureBasket(EntityTypeBuilder<Basket> builder)
        {
            var navigation = builder.Metadata.FindNavigation(nameof(Basket.Items));

            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);
        }

        private void ConfigureItem(EntityTypeBuilder<Item> builder)
        {
            builder.ToTable(nameof(Item));

            //builder.Property(ci => ci.Id)
            //    .ForSqlServerUseSequenceHiLo("item_hilo")
            //    .IsRequired();

            builder.Property(ci => ci.Title)
                .IsRequired(true)
                .HasMaxLength(50);

            builder.Property(ci => ci.Price)
                .IsRequired(true);


            builder.HasOne(ci => ci.MeasurementUnit)
                .WithMany()
                .HasForeignKey(ci => ci.MeasurementUnitId);

            builder.HasOne(ci => ci.Brand)
                .WithMany()
                .HasForeignKey(ci => ci.BrandId);

            builder.HasOne(ci => ci.Category)
                .WithMany()
                .HasForeignKey(ci => ci.CategoryId);

            builder.HasOne(ci => ci.Store)
                .WithMany()
                .HasForeignKey(ci => ci.StoreId);

            builder.HasMany(i => i.Images).WithOne(i => i.RelatedEntity).HasForeignKey(i => i.Id);
        }
        private void ConfigureBrand(EntityTypeBuilder<Brand> builder)
        {
            builder.ToTable(nameof(Brand));

            builder.HasKey(ci => ci.Id);

            //builder.Property(ci => ci.Id)
            //   .ForSqlServerUseSequenceHiLo("brand_hilo")
            //   .IsRequired();

            builder.Property(cb => cb.Title)
                .IsRequired()
                .HasMaxLength(100);
        }
        private void ConfigureCategory(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable(nameof(Category));

            builder.HasKey(ci => ci.Id);

            //builder.Property(ci => ci.Id)
            //   .ForSqlServerUseSequenceHiLo("category_hilo")
            //   .IsRequired();

            builder.Property(cb => cb.Title)
                .IsRequired()
                .HasMaxLength(100);
        }
        private void ConfigureOrder(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable(nameof(Order));

            var navigation = builder.Metadata.FindNavigation(nameof(Order.OrderItems));

            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.OwnsOne(o => o.ShipToAddress);
        }
        private void ConfigureOrderItem(EntityTypeBuilder<OrderItem> builder)
        {
            builder.OwnsOne(i => i.ItemOrdered);
        }
        private void ConfigureMeasurementUnit(EntityTypeBuilder<MeasurementUnit> builder)
        {
            builder.ToTable(nameof(MeasurementUnit));
            builder.HasKey(ci => ci.Id);
            //builder.Property(ci => ci.Id)
            //    .ForSqlServerUseSequenceHiLo("measurementUnit_hilo")
            //    .IsRequired();
            builder.Property(cb => cb.Title)
                .IsRequired()
                .HasMaxLength(100);
        }
        private void ConfigureStore(EntityTypeBuilder<Store> builder)
        {
            builder.ToTable(nameof(Store));
            builder.HasKey(ci => ci.Id);
            //builder.Property(ci => ci.Id)
            //    .ForSqlServerUseSequenceHiLo("store_hilo")
            //    .IsRequired();
            builder.Property(cb => cb.Title)
                .IsRequired()
                .HasMaxLength(100);
        }
        private void ConfigureItemImage(EntityTypeBuilder<FileDetails<Item>> builder)
        {
            builder.ToTable("ImageItem");
            builder.HasKey(ci => ci.Id);
        }
    }
}
