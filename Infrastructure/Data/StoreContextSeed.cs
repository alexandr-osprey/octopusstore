﻿using ApplicationCore.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class StoreContextSeed
    {
        public static async Task SeedAsync(StoreContext storeContext, ILoggerFactory loggerFactory, int? retry = 0)
        {
            int retryForAvailabiltiy = retry.Value;
            try
            {
                if(!storeContext.MeasurementUnits.Any())
                {
                    storeContext.MeasurementUnits.AddRange(GetPreconfiguredMeasurementUnits());
                    await storeContext.SaveChangesAsync();
                }
                if (!storeContext.Brands.Any())
                {
                    storeContext.Brands.AddRange(GetPreconfiguredBrands());
                    await storeContext.SaveChangesAsync();
                }
                if (!storeContext.Categories.Any())
                {
                    storeContext.Categories.AddRange(GetPreconfiguredCategories());
                    await storeContext.SaveChangesAsync();
                }
                if (!storeContext.Items.Any())
                {
                    storeContext.Items.AddRange(GetPreconfiguredItems());
                    await storeContext.SaveChangesAsync();
                }
                if (!storeContext.Stores.Any())
                {
                    storeContext.Stores.AddRange(GetPreconfiguredStores());
                    await storeContext.SaveChangesAsync();
                }
                if (!storeContext.ItemsImages.Any())
                {
                    storeContext.ItemsImages.AddRange(GetPreconfiguredItemImagesAsync().Result);
                    storeContext.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                if (retryForAvailabiltiy < 10)
                {
                    retryForAvailabiltiy++;
                    var log = loggerFactory.CreateLogger<StoreContextSeed>();
                    log.LogError(ex.Message);
                    await SeedAsync(storeContext, loggerFactory, retryForAvailabiltiy);
                }
            }
        }

        static IEnumerable<MeasurementUnit> GetPreconfiguredMeasurementUnits()
        {
            return new List<MeasurementUnit>()
            {
                new MeasurementUnit { Title = "m" },
                new MeasurementUnit { Title = "kg" },
                new MeasurementUnit { Title = "pcs" }
            };
        }

        static IEnumerable<Brand> GetPreconfiguredBrands()
        {
            return new List<Brand>
            {
                new Brand { Title = "Apple" },
                new Brand { Title = "Samsung" },
                new Brand { Title = "CK" },
                new Brand { Title = "Armani" }
            };
        }

        static IEnumerable<Category> GetPreconfiguredCategories()
        {
            return new List<Category>
            {
                new Category { Title = "root", CanHaveItems = false },
                new Category { Title = "Electronics", CanHaveItems = false, ParentCategoryId = 1 },
                new Category { Title = "Clothes", CanHaveItems = false, ParentCategoryId = 1}
            };
        }

        static IEnumerable<Store> GetPreconfiguredStores()
        {
            return new List<Store>
            {
                new Store { Title = "John's store", Address = "NY", Description = "Electronics best deals", SellerId = "john@mail.com" },
                new Store { Title = "Jennifer's store", Address = "Sydney", Description = "Fashion", SellerId = "jennifer@mail.com" }
            };
        }

        static IEnumerable<Item> GetPreconfiguredItems()
        {
            return new List<Item>()
            {
                new Item { Title = "iPhone 6", BrandId = 1, CategoryId = 2, MeasurementUnitId = 3, Price = 1000, StoreId = 1 },
                new Item { Title = "Samsung 7", BrandId = 2, CategoryId = 2, MeasurementUnitId = 3, Price = 700, StoreId = 1 },
                new Item { Title = "Samsung 8", BrandId = 2, CategoryId = 2, MeasurementUnitId = 3, Price = 800, StoreId = 1 },

                new Item { Title = "Shoes", BrandId = 3, CategoryId = 3, MeasurementUnitId = 3, Price = 500, StoreId = 2  },
                new Item { Title = "Jacket", BrandId = 4, CategoryId = 3, MeasurementUnitId = 3, Price = 800, StoreId = 2  },
            };
        }

        static async Task<IEnumerable<FileDetails<Item>>> GetPreconfiguredItemImagesAsync()
        {
            ClearFilesDirectory();
            string contentType = @"image/jpg";
            string imagesDirectory = @"C:\images";
            var images = new List<FileDetails<Item>>
            {
                new FileDetails<Item> ("john@mail.com", contentType, 1) { Title = "iPhone 6" },
                new FileDetails<Item> ("john@mail.com", contentType, 2) { Title = "Samsung 7" },
                new FileDetails<Item> ("john@mail.com", contentType, 3) { Title = "Samsung 8" },
                new FileDetails<Item> ("jennifer@mail.com", contentType, 4) { Title = "Shoes" },
                new FileDetails<Item> ("jennifer@mail.com", contentType, 5) { Title = "Jacket" }
            };
            foreach (var image in images)
            {
                image.SaveFileAsync(
                    File.OpenRead(
                        Path.Combine(imagesDirectory, image.Title + ".jpg"))).Wait();
            };
            return images;
        }
        static void ClearFilesDirectory()
        {
            DirectoryInfo di = new DirectoryInfo(FileDetails<Item>.rootDirectory);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
        }
    }
}
