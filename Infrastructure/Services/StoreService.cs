﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class StoreService : IStoreService
    {
        private readonly IAsyncRepository<Store> _storeRepository;

        public StoreService(IAsyncRepository<Store> storeRepository)
        {
            _storeRepository = storeRepository;
        }

        public async Task<Store> AddAsync(Store store)
        {
            return await _storeRepository.AddAsync(store);
        }
        public async Task<Store> GetByIdAsync(int id)
        {
            return await _storeRepository.GetByIdAsync(id);
        }
        public async Task UpdateAsync(Store store)
        {
            await _storeRepository.UpdateAsync(store);
        }
        public async Task DeleteAsync(int id)
        {
            var store = await _storeRepository.GetByIdAsync(id);
            if (store == null)
            {
                return;
            }
            await _storeRepository.DeleteAsync(store);
        }
        public async Task<IEnumerable<Store>> ListAllAsync()
        {
            return await _storeRepository.ListAllAsync();
        }
    }
}
