﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Specifications;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IAsyncRepository<Category> _categoryRepository;

        public CategoryService(IAsyncRepository<Category> categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<Category> GetByIdAsync(int id)
        {
            return await _categoryRepository.GetByIdAsync(id);
        }
        public async Task<IEnumerable<Category>> ListSubcategoriesAsync(int parentId)
        {
            return await _categoryRepository.ListAsync(new SubcategoriesFilterSpecification(parentId));
        }
        public async Task<IEnumerable<Category>> ListRootCategoriesAsync()
        {
            return await ListSubcategoriesAsync(1);
        }
    }
}
