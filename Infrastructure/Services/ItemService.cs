﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class ItemService : IItemService
    {
        private readonly IAsyncRepository<Item> _itemRepository;

        public ItemService(IAsyncRepository<Item> itemRepository)
        {
            _itemRepository = itemRepository;
        }
        public async Task<IEnumerable<Item>> ListAsync(int pageIndex, int itemsPage, ISpecification<Item> spec)
        {
            var root = await _itemRepository.ListAsync(spec);

            var totalItems = root.Count();

            return root.Skip(itemsPage * (pageIndex - 1))
                .Take(itemsPage);
        }
        public async Task<Item> GetByIdAsync(int id)
        {
            return await _itemRepository.GetByIdAsync(id);
        }
        public async Task<Item> AddAsync(Item entity)
        {
            return await _itemRepository.AddAsync(entity);
        }
        public async Task UpdateAsync(Item entity)
        {
            await _itemRepository.UpdateAsync(entity);
        }
        public async Task DeleteAsync(int id)
        {
            var item = await _itemRepository.GetByIdAsync(id);
            if (item == null)
            {
                return;
            }
            await _itemRepository.DeleteAsync(item);
        }

    }
}
