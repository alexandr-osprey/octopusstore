﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class BrandService : IBrandService
    {
        private readonly IAsyncRepository<Brand> _brandRepository;

        public BrandService(IAsyncRepository<Brand> brandRepository)
        {
            _brandRepository = brandRepository;
        }

        public async Task<IEnumerable<Brand>> ListAllAsync()
        {
            return await _brandRepository.ListAllAsync();
        }
    }
}
