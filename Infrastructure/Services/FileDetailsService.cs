﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Specifications;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class FileDetailsService <T> : IFileDetailsService<T>
        where T : BaseEntity
    {
        private readonly IAsyncRepository<FileDetails<T>> _fileDetailsRepository;

        public FileDetailsService(IAsyncRepository<FileDetails<T>> fileDetailsRepository)
        {
            _fileDetailsRepository = fileDetailsRepository;
        }

        public async Task<Stream> GetStreamAsync(int id)
        {
            var fileDetails = await _fileDetailsRepository.GetByIdAsync(id);
            if (fileDetails == null)
            {
                return null;
            }
            return fileDetails.GetStream();
        }
        public async Task<FileDetails<T>> GetByIdAsync(int id)
        {
            var fileDetails = await _fileDetailsRepository.GetByIdAsync(id);
            if (fileDetails == null)
            {
                return null;
            }
            return fileDetails;
        }
        public async Task<IEnumerable<FileDetails<T>>> ListByRelatedIdAsync(int relatedId)
        {
            var files = await _fileDetailsRepository.ListAsync(new FileByRelatedIdSpecification<T>(relatedId));
            return files;
        }
        public async Task<FileDetails<T>> AddAsync(FileDetails<T> imageDetails, Stream fileStream)
        {
            await _fileDetailsRepository.AddAsync(imageDetails);
            await imageDetails.SaveFileAsync(fileStream);
            return imageDetails;
        }
        public async Task DeleteAsync(int id)
        {
            var fileDetails = await _fileDetailsRepository.GetByIdAsync(id);
            if (fileDetails == null)
            {
                return;
            }
            await _fileDetailsRepository.DeleteAsync(fileDetails);
        }


    }
}
