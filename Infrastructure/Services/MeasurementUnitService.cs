﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class MeasurementUnitService : IMeasurementUnitService
    {
        private readonly IAsyncRepository<MeasurementUnit> _measurementUnitRepository;

        public MeasurementUnitService(IAsyncRepository<MeasurementUnit> measurementUnitRepository)
        {
            _measurementUnitRepository = measurementUnitRepository;
        }

        public async Task<MeasurementUnit> GetByIdAsync(int id)
        {
            return await _measurementUnitRepository.GetByIdAsync(id);
        }
        public async Task<IEnumerable<MeasurementUnit>> ListAllAsync()
        {
            return await _measurementUnitRepository.ListAllAsync();
        }
    }
}
